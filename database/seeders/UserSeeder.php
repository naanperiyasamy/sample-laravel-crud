<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Faker\Factory as Faker;
use Carbon\Carbon;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
    	foreach (range(1,10) as $index) {
			$startDate = Carbon::createFromTimeStamp($faker->dateTimeBetween('-100 days', '-30 days')->getTimestamp());
			$endDate = Carbon::createFromFormat('Y-m-d H:i:s', $startDate)->addDays('15');

	        DB::table('users')->insert([
	            'name' => $faker->name,
	            'email' => $faker->email,
	            'address' => $faker->address,
	            'created_at' => $startDate,
	            'updated_at' => $endDate
	        ]);
		}
    }
}
