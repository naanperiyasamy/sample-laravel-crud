<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Validator;
use Mail;

class UsersController extends Controller
{
    public function index(Request $request)
    {
        if (!$request->isMethod('get') || !$request->is('users')) {
            abort(403, 'Unauthorized action.');
        }
        $users = User::paginate(5);
        return view('users.list',compact('users'));
    }

    public function create(Request $request)
    {
        if (!$request->isMethod('get') || !$request->is('users/create')) {
            abort(403, 'Unauthorized action.');
        }
        return view('users.create');
    }

    public function store(Request $request)
    {
        if (!$request->isMethod('post') || !$request->is('users')) {
            abort(403, 'Unauthorized action.');
        }
        $input = $request->all();

        $validator = Validator::make($input, [
            'name' => 'required|min:3|max:50',
            'email' => 'required|email|unique:users',
            'address' => 'required|min:8|max:100'
        ]);

        if ($validator->fails()) {
            return redirect('/users/create')->withInput()->withErrors($validator->messages());
        }

        $user = new User();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->address = $request->address;

        if ($user->save()) {
            session()->flash('success', 'New user is added.');
            return redirect('/users');
        } else {
            session()->flash('danger', 'Failed to add new user.');
            return redirect('/users/create');
        }
    }

    public function edit(Request $request, $id)
    {
        if (!$request->isMethod('get') || !$request->is('users/*/edit')) {
            abort(403, 'Unauthorized action.');
        }
        $user = User::find($id);

        if (!$user) {
            session()->flash('danger', 'User not found');
            return redirect('/users');
        }

        return view('users.edit',compact('user'));
    }

    public function update($id, Request $request)
    {
        if (!$request->isMethod('put') || !$request->is('users/*')) {
            abort(403, 'Unauthorized action.');
        }
        $user = User::find($id);

        $input = $request->all();

        if (!$user) {
            session()->flash('danger', 'User not found');
            return redirect('/users');
        }

        $validator = Validator::make($input, [
            'name' => 'required|min:3|max:50',
            'email' => 'required|email|unique:users,email,'.$user->id,
            'address' => 'required|min:8|max:100'
        ]);

        if ($validator->fails()) {
            return redirect('/users/'.$id.'/edit')->withErrors($validator->messages());
        }

        $user->name = $request->name;
        $user->email = $request->email;
        $user->address = $request->address;

        if ($user->save()) {
            session()->flash('success', 'User detail is updated.');
            return redirect('/users');
        } else {
            session()->flash('danger', 'Failed to update the user details.');
            return redirect('/users/'.$id.'/edit');
        }
    }

    public function destroy($id, Request $request)
    {
        if (!$request->isMethod('delete') || !$request->is('users/*')) {
            abort(403, 'Unauthorized action.');
        }
        $user = User::find($id);

        if (!$user) {
            session()->flash('danger', 'User not found');
            return redirect('/users');
        }

        if ($user->delete()) {
            session()->flash('success', 'User is deleted.');
            return redirect('/users');
        } else {
            session()->flash('danger', 'Failed to delete the user details.');
            return redirect('/users');
        }
    }

    public function message($id, Request $request)
    {
        if (!$request->isMethod('get') || !$request->routeIs('user-message')) {
            abort(403, 'Unauthorized action.');
        }
        $user = User::find($id);

        if (!$user) {
            session()->flash('danger', 'User not found');
            return redirect('/users');
        }

        return view('users.send',compact('user'));
    }

    public function send($id, Request $request)
    {
        if (!$request->isMethod('post') || !$request->routeIs('user-mail')) {
            abort(403, 'Unauthorized action.');
        }
        $user = User::find($id);

        if (!$user) {
            session()->flash('danger', 'User not found');
            return redirect('/users');
        }

        $input = $request->all();
        $input['email'] = $user->email;
        $input['name'] = $user->name;

        $validator = Validator::make($input, [
            'subject' => 'required|min:3|max:25',
            'message' => 'required|min:8|max:100'
        ]);

        if ($validator->fails()) {
            return redirect('/users/'.$id.'/send')->withInput()->withErrors($validator->messages());
        }

        Mail::send('layouts.mail', ['user' => $input], function ($message) use ($input) {
            $message->from(env('MAIL_FROM_ADDRESS'),env('MAIL_FROM_NAME'));
            $message->to($input['email']);
            $message->subject($input['subject']);
        });

        session()->flash('success', 'Email sent to '.$user->name);
        return redirect('/users');
    }
}
