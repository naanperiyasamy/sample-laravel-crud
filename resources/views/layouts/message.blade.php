
@if (Session::has('danger'))
    <div class="p-1">
        <div x-data="{ show: true }" x-show="show"
            class="flex justify-between items-center bg-red-200 relative text-yellow-600 py-1 px-3 rounded-lg">
            <div>
                {!! session('danger') !!}
            </div>
            <div>
                <button type="button" @click="show = false" class=" text-yellow-700">
                    <span class="text-2xl">&times;</span>
                </button>
            </div>
        </div>
    </div>
@endif

@if (Session::has('success'))
    <div class="p-1">
        <div x-data="{ show: true }" x-show="show"
            class="flex justify-between items-center bg-green-200 relative text-yellow-600 py-1 px-3 rounded-lg">
            <div>
                {!! session('success') !!}
            </div>
            <div>
                <button type="button" @click="show = false" class=" text-yellow-700">
                    <span class="text-2xl">&times;</span>
                </button>
            </div>
        </div>
    </div>
@endif

@if (count($errors))

    @foreach ($errors->all() as $error)
        <div class="p-1">
            <div x-data="{ show: true }" x-show="show"
                class="flex justify-between items-center bg-red-200 relative text-yellow-600 py-1 px-3 rounded-lg">
                <div>
                    {{-- <span class="font-semibold text-yellow-700">Bummer !!!</span> --}}
                    {{ $error }}
                </div>
                <div>
                    <button type="button" @click="show = false" class=" text-yellow-700">
                        <span class="text-2xl">&times;</span>
                    </button>
                </div>
            </div>
        </div>
    @endforeach

@endif