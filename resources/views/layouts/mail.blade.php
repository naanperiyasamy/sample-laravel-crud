<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Email</title>

    <!-- Styles -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/tailwindcss/0.7.4/tailwind.min.css" rel="stylesheet">

</head>
<body class="bg-gray-100 text-gray-900 tracking-wider leading-normal">
<div class="app font-sans min-w-screen min-h-screen bg-grey-lighter py-8 px-4">
  
  <div class="mail__wrapper max-w-md mx-auto">
   
    <div class="mail__content bg-white p-8 shadow-md">

      <div class="content__header h-64 flex flex-col items-center justify-center text-center tracking-wide leading-normal bg-black -mx-8 -mt-8">
          <h1 class="text-red text-5xl">Periyasamy</h1>
          <p class="text-white text-2xl">{{ $user['subject'] }}!</p>
      </div>

      <div class="content__body py-8 border-b">
        <p>
          Hey {{ $user['name'] }}!<br><br>
          {{ $user['message'] }}
        </p>
        <button class="text-white text-sm tracking-wide bg-red rounded w-full my-8 p-4 " type="button">View Sample Application</button>
        <p class="text-sm">
          Keep Rockin'!<br>
          Your The App team
        </p>
      </div>

      <div class="content__footer mt-8 text-center text-grey-darker">
        <h3 class="text-base sm:text-lg mb-4">Thanks for using The App!</h3>
        <p>www.test.local</p>
      </div>

    </div>

  </div>
</div>
</body>
</html>
