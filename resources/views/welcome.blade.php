<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="{{ mix('css/app.css') }}" rel="stylesheet">
</head>
<body class="h-full text-white" style="background-color: #667eea">

    <section style="background-color: #667eea">
        <div class="container mx-auto px-6 text-center py-20">
            <h2 class="mb-6 text-4xl font-bold text-center text-white">
            Sample Users List
            </h2>
            <h3 class="my-4 text-2xl text-white">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit!
            </h3>
            <a href="/users">
                <button class="bg-red bg-font-bold rounded-full mt-6 py-4 px-8 shadow-lg uppercase tracking-wider">
                Click here to view
                </button>
            </a>
        </div>
    </section>

</body>
</html>
