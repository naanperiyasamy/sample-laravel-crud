@extends('layouts.app')

@section('content')

    <div class="relative w-full flex flex-col h-screen overflow-y-hidden">
        <div class="w-full h-screen overflow-x-hidden border-t flex flex-col">
            <main class="w-full flex-grow p-6">

                <div class="flex flex-wrap">
                    <div class="container w-full md:w-4/5 xl:w-3/5 mx-auto px-2">
                        <h1 class="flex items-center font-sans font-bold break-normal text-indigo-500 px-2 py-8 text-xl md:text-2xl">
                            Send Message to {{ $user->name }}
                        </h1>
                        <div class="leading-loose">
                            <form method="POST" class="p-10 bg-white rounded shadow-xl">

                                @include ('layouts.message')

                                {{ csrf_field() }}

                                <div class="">
                                    <label class="block text-sm text-gray-600" for="subject">Subject</label>
                                    <input class="w-full px-5 py-1 text-gray-700 bg-gray-200 rounded" id="subject" name="subject" type="text" required="" placeholder="Enter Subject" aria-label="Subject" value="{{ old('subject') }}">
                                </div>
                                <div class="mt-2">
                                    <label class=" block text-sm text-gray-600" for="message">Message</label>
                                    <textarea class="w-full px-5 py-2 text-gray-700 bg-gray-200 rounded" id="message" name="message" rows="6" required="" placeholder="Enter Message" aria-label="Message">{{ old('message') }}</textarea>
                                </div>
                                <div class="mt-6">
                                    <button class="px-4 py-1 text-white font-light tracking-wider bg-green-500 rounded" type="submit">Send</button>
                                    <a href="/users"><button class="px-4 py-1 text-white font-light tracking-wider bg-gray-500 rounded" type="button">Cancel</button></a>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </main>
        </div>
    </div>

    @push('scripts')
    @endpush

@endsection
