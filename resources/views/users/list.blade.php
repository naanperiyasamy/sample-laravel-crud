@extends ('layouts.app')

@section ('content')
    <div class="container w-full md:w-4/5 xl:w-4/5 mx-auto px-2">

        <!--Title-->
        <h1 class="flex items-center font-sans font-bold break-normal text-indigo-500 px-2 py-8 text-xl md:text-2xl">
            Users List
        </h1>

        <div id='recipients' class="p-8 mt-6 lg:mt-0 rounded shadow bg-white">
        
        <div class="p-1">            
            <a href="/users/create"><button class="px-4 py-1 text-white font-light tracking-wider bg-blue-500 rounded" type="button">Add User</button></a><br/>
        </div>

            @include ('layouts.message')

            @if (count($users) == 0)
                <h2 class="text-center">No Users found </h2>
            @else
                <table id="example1" class="w-full">
                    <thead>
                        <tr>
                            <th class="bg-blue-100 border text-left px-8 py-4">Name</th>
                            <th class="bg-blue-100 border text-left px-8 py-4">Email</th>
                            <th class="bg-blue-100 border text-left px-8 py-4">Address</th>
                            <th class="bg-blue-100 border text-left px-8 py-4">Created At</th>
                            <th class="bg-blue-100 border text-left px-8 py-4">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($users as $user)
                            <tr>
                                <td class="border py-4 p-2">{{ $user->name }}</td>
                                <td class="border py-4 p-2">{{ $user->email }}</td>
                                <td class="border py-5 p-2">{{ $user->address }}</td>
                                <td class="border px-3 py-3 p-2">{{ \Carbon\Carbon::parse($user->created_at)->format('m/d/Y h:i A') }}</td>
                                <td class="border py-4">
                                    <div class="flex item-center justify-center">
                                        <div class="w-4 mr-2 transform hover:text-purple-500 hover:scale-110">
                                            <a href="/users/{{ $user->id }}/send">
                                                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 20 20" stroke="currentColor">
                                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M17.218,2.268L2.477,8.388C2.13,8.535,2.164,9.05,2.542,9.134L9.33,10.67l1.535,6.787c0.083,0.377,0.602,0.415,0.745,0.065l6.123-14.74C17.866,2.46,17.539,2.134,17.218,2.268 M3.92,8.641l11.772-4.89L9.535,9.909L3.92,8.641z M11.358,16.078l-1.268-5.613l6.157-6.157L11.358,16.078z"/>
                                                </svg>
                                            </a>
                                        </div>
                                        <div class="w-4 mr-2 transform hover:text-green-500 hover:scale-110">
                                            <a href="/users/{{ $user->id }}/edit">
                                                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M15.232 5.232l3.536 3.536m-2.036-5.036a2.5 2.5 0 113.536 3.536L6.5 21.036H3v-3.572L16.732 3.732z" />
                                                </svg>
                                            </a>
                                        </div>
                                        <form method="POST" action="/users/{{ $user->id }}">
                                            {{ csrf_field() }}
                                            @method('DELETE')
                                            <button type="submit">
                                                <div class="w-4 mr-2 transform hover:text-red-500 hover:scale-110">
                                                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 7l-.867 12.142A2 2 0 0116.138 21H7.862a2 2 0 01-1.995-1.858L5 7m5 4v6m4-6v6m1-10V4a1 1 0 00-1-1h-4a1 1 0 00-1 1v3M4 7h16" />
                                                </svg>
                                                </div>
                                            </button>
                                        </form>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>

                <div class="p-4 w-full">
                    {{ $users->links('pagination::tailwind') }}
                </div>
            @endif
        </div>
    </div>

    @push ('scripts')


    @endpush

@endsection