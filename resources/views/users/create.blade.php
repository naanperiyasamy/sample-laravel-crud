@extends('layouts.app')

@section('content')

    {{-- <div class="container w-full md:w-4/5 xl:w-3/5  mx-auto px-2"> --}}
    <div class="relative w-full flex flex-col h-screen overflow-y-hidden">
        <div class="w-full h-screen overflow-x-hidden border-t flex flex-col">
            <main class="w-full flex-grow p-6">
                {{-- <h1 class="w-full text-3xl text-black pb-6">Forms</h1> --}}

                <div class="flex flex-wrap">
                    <div class="container w-full md:w-4/5 xl:w-3/5 mx-auto px-2">
                    {{-- <div class="w-full md:w-4/5 xl:w-3/5 wl-4 my-6 pr-0 lg:pr-2"> --}}
                        <h1 class="flex items-center font-sans font-bold break-normal text-indigo-500 px-2 py-8 text-xl md:text-2xl">
                            New User
                        </h1>
                        <div class="leading-loose">
                            <form method="POST" action="/users" class="p-10 bg-white rounded shadow-xl">

                                @include ('layouts.message')

                                {{ csrf_field() }}

                                <div class="">
                                    <label class="block text-sm text-gray-600" for="name">Name</label>
                                    <input class="w-full px-5 py-1 text-gray-700 bg-gray-200 rounded" id="name" name="name" type="text" required="" placeholder="Enter Name" aria-label="Name" value="{{ old('name') }}">
                                </div>
                                <div class="mt-2">
                                    <label class="block text-sm text-gray-600" for="email">Email</label>
                                    <input class="w-full px-5  py-4 text-gray-700 bg-gray-200 rounded" id="email" name="email" type="text" required="" placeholder="Enter Email" aria-label="Email" value="{{ old('email') }}">
                                </div>
                                <div class="mt-2">
                                    <label class=" block text-sm text-gray-600" for="address">Address</label>
                                    <textarea class="w-full px-5 py-2 text-gray-700 bg-gray-200 rounded" id="address" name="address" rows="6" required="" placeholder="Enter Address.." aria-label="Address">{{ old('address') }}</textarea>
                                </div>
                                <div class="mt-6">
                                    <button class="px-4 py-1 text-white font-light tracking-wider bg-green-500 rounded" type="submit">Submit</button>
                                    <a href="/users"><button class="px-4 py-1 text-white font-light tracking-wider bg-gray-500 rounded" type="button">Cancel</button></a>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </main>
        </div>
    </div>

    @push('scripts')
    @endpush

@endsection
